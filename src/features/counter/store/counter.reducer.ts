import { decrement, increment } from './counter.actions';
import { createReducer } from '@reduxjs/toolkit';

export const counterReducer = createReducer(0, {
  [increment.type]: (state, action) => {
    return state + action.payload;
  },
  [decrement.type]: (state, action) => {
    return state - action.payload;
  },
});

// OLD VERSION (not typed)
export function counterReducerOLD(state = 0, action: any) {
  switch (action.type) {
    case increment.type:
      return state + action.payload;
    case decrement.type:
      return state - action.payload;
    default:
      return state
  }
}


