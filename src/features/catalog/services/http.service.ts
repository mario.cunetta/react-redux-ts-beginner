export function getHeaders() {
  return {
    "Content-Type": "application/json",
  };
}


// =========================
// HTTP / Fetch utility
// =========================

export async function get(url: string) {
  const response = await fetch(url, {
    method: 'GET',
  });
  return response.json();
}

export async function patch(url: string, data: any) {
  const response = await fetch(url,
    {
      method: 'PATCH',
      headers: getHeaders(),
      body: JSON.stringify(data)
    },
  );
  return response.json();
}

export async function post(url: string, data?: any) {
  const params: any = {};
  params.method = 'POST';
  params.headers = getHeaders();
  if (data) {
    params.body = JSON.stringify(data);
  }
  const response = await fetch(url,params);
  return response.json();
}

export async function del(url: string) {
  const response = await fetch(url,
    {
      method: 'DELETE',
      headers: getHeaders(),
    },
  );
  return response.json();
}
