import { RootState } from '../../../App';
import { Product } from '../model/product';

export const getProducts = (state: RootState) => state.catalog;
export const getTotalProducts = (state: RootState) => state.catalog.reduce((acc: number, cur: Product) => {
  return acc + cur.price;
}, 0);
